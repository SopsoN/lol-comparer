import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PageSearchBlockComponent } from './page-search-block.component';

describe('PageSearchBlockComponent', () => {
  let component: PageSearchBlockComponent;
  let fixture: ComponentFixture<PageSearchBlockComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PageSearchBlockComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PageSearchBlockComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
