import { Component, OnInit } from '@angular/core';

import { Router } from '@angular/router';

@Component({
  selector: 'app-page-search-block',
  templateUrl: './page-search-block.component.html',
  styleUrls: ['./page-search-block.component.css']
})
export class PageSearchBlockComponent implements OnInit {
    summonerName = "";
    summonerRegion = "EUNE";

    constructor(private router: Router) { }

    ngOnInit() {
    }

    searchInputKeyUp(value: string) {
        this.summonerName = value;
    }

    searchSelectClick(value: string) {
        this.summonerRegion = value;
    }

    searchURL: string;

    onSubmit() {
        this.summonerName = this.summonerName.split(' ').join('');
        this.searchURL = "/search/region/"+this.summonerRegion+"/name/"+this.summonerName;
        this.router.navigateByUrl(this.searchURL);
        window.location.reload(true);
    }
}
