import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PageCompareBoxComponent } from './page-compare-box.component';

describe('PageCompareBoxComponent', () => {
  let component: PageCompareBoxComponent;
  let fixture: ComponentFixture<PageCompareBoxComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PageCompareBoxComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PageCompareBoxComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
