import { Component, OnInit } from '@angular/core';

import { Router, ActivatedRoute } from '@angular/router';
import { HttpClient } from '@angular/common/http';

import { Item } from '../riotApiClasses';

@Component({
  selector: 'app-page-single-item',
  templateUrl: './page-single-item.component.html',
  styleUrls: ['./page-single-item.component.css', '../app.component.css']
})
export class PageSingleItemComponent implements OnInit {
    item: Item;
    name: string;
    nameParam: string;
    url: string;

    version: string;
    versionUrl: string;

    pageIsLoaded = false;

    constructor(private route: ActivatedRoute, private http: HttpClient) { }

    ngOnInit() {
        this.name = this.route.snapshot.paramMap.get('name');
        console.log(this.name);
        this.versionUrl = "./assets/riotApiCommunicator.php?request=version";
        this.http.get<string>(this.versionUrl).subscribe(data => this.version = data);
        this.getItem();
    }

    getItem() {
        this.nameParam = this.name.split(' ').join('_');
        this.url = "./assets/riotApiCommunicator.php?request=getItem&itemName="+this.nameParam;
        console.log(this.url);
        this.http.get<Item>(this.url).subscribe(resp => {
            this.item = resp;
            this.item.description = this.item.description.replace(/<\/?[^>]+(>|$)/g, "");
            console.log(resp);
            this.pageIsLoaded = true;
        });
    }

}
