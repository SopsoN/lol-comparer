import { Component, OnInit } from '@angular/core';

import { Router, ActivatedRoute } from '@angular/router';
import { HttpClient } from '@angular/common/http';

import { SinglePlayerContainer, Champion, comparePlayersClass } from '../riotApiClasses';
import { Chart } from 'chart.js'

import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { CompareBoxServiceService } from '../compare-box-service.service';

@Component({
  selector: 'app-page-single-player',
  templateUrl: './page-single-player.component.html',
  styleUrls: ['./page-single-player.component.css', '../app.component.css'],
  providers: [ CompareBoxServiceService ]
})

export class PageSinglePlayerComponent implements OnInit {

    name = "";
    region = "";

    pageIsLoaded = false;

    url: string;
    sPlayer: SinglePlayerContainer;

    champions: Champion[];

    version: string;
    versionUrl: string

    positionsChart = [];
    winLosesChart = [];
    killDeathsAssistsChart = [];

    summonerSpells = {
        "1": "SummonerBoost",
        "3": "SummonerExhaust",
        "4": "SummonerFlash",
        "6": "SummonerHaste",
        "7": "SummonerHeal",
        "11": "SummonerSmite",
        "12": "SummonerTeleport",
        "13": "SummonerMana",
        "14": "SummonerDot",
        "21": "SummonerBarrier",
        "30": "SummonerPoroRecall",
        "31": "SummonerPoroThrow",
        "32": "SummonerSnowball",
    };

    lolMaps = {
        "11": "Summoner's Rift",
        "12": "Howling Abyss",
        "14": "Butcher's Bridge",
        "16": "Cosmic Ruins",
        "18": "Valoran City Park",
        "8": "The Crystal Scar",
        "19": "Substructure 43",
        "10": "Twisted Treeline"
    }

    typeGame = {
        "400": "5v5 Draft Pick games",
        "420": "5v5 Ranked Solo/Duo games",
        "430": "5v5 Blind Pick games",
        "440": "5v5 Ranked Flex games",
        "450": "5v5 ARAM games",
    }

    compareTwoPlayers: comparePlayersClass;

    constructor(
        private route: ActivatedRoute,
        private router: Router,
        private http: HttpClient,
        private recivedPlayers: CompareBoxServiceService
    ) {}

    ngOnInit() {
        this.sopsonOnInit();
    }

    sopsonOnInit(name = "") {

        if(name == "")
        {
            this.name = this.route.snapshot.paramMap.get('name');
            this.name = this.name.split(' ').join('');
        }
        else
        {
            this.name = name.split(' ').join('');
        }

        this.region = this.route.snapshot.paramMap.get('region');

        this.recivedPlayers.currentMessage.subscribe(message => this.compareTwoPlayers = message);

        this.versionUrl = "./assets/riotApiCommunicator.php?request=version";
        this.http.get<string>(this.versionUrl).subscribe(data => this.version = data);

        this.getPlayer();
    }

    getPlayer() {
        this.url = "./assets/riotApiCommunicator.php?request=getPlayer&summoner_name="+this.name+"&region="+this.region;

        this.http.get<SinglePlayerContainer>(this.url).subscribe(resp => {
            console.log(resp);
            this.sPlayer = resp;

            let player_id;
            let player_kills = 0;
            let player_deaths = 0;
            let player_assists = 0;
            let player_wins = 0;
            let player_loses = 0;

            for(let match of this.sPlayer.display_matches)
            {
                for(let inGamePlayer of match.participantIdentities)
                {
                    if(inGamePlayer.player.summonerName == this.name)
                    {
                        player_id = inGamePlayer.participantId;
                        break;
                    }
                }
                let player_team = match.participants[player_id-1].teamId;

                if(match.teams[player_team/100-1].win == "Win")
                {
                    player_wins++;
                }
                else if(match.teams[player_team/100-1].win == "Fail")
                {
                    player_loses++;
                }

                player_kills += match.participants[player_id-1].stats.kills;
                player_deaths += match.participants[player_id-1].stats.deaths;
                player_assists += match.participants[player_id-1].stats.assists;
            }

            Chart.defaults.global.defaultFontColor = "#fff";

            this.positionsChart = new Chart('positionsChart', {
                type: 'radar',
                scaleFontColor: "#FFFFFF",
                data: {
                    labels: [
                        "TOP",
                        "JUNGLE",
                        "MID",
                        "BOTTOM",
                        "SUPPORT"
                    ],
                    defaultColor: "#FFF",
                    datasets: [{
                        data: [
                            this.sPlayer.positions.top,
                            this.sPlayer.positions.jungle,
                            this.sPlayer.positions.mid,
                            this.sPlayer.positions.bottom,
                            this.sPlayer.positions.support
                        ],
                        BackgroundColor: "#DF691A",
                        borderColor: "#FFF",
                        pointBackgroundColor: "#DF691A",
                        pointBorderColor: "#DF691A",
                        pointLabelFontColor: "#DF691A",
                        borderWidth: 1,
                        color:"#DF691A",
                        fillColor: "#FFF",
                        fontColor: "#FFF",
                        scaleFontColor: "#FFFFFF",
                        fill: false
                    }]
                },
                options: {
                    responsive: true,
                    legend: {
                        display: false,
                    },
                    elements: {
                        line: {
                            borderColor: "#DF691A",
                            backgroundColor: "#DF691A"
                        }
                    },
                    scale: {
                        Axes: {
                            ticks: {
                                fontColor: "white",
                                fontSize: 18,
                                stepSize: 1,
                                beginAtZero: true
                            }
                        },
                        ticks: {
                            display: false,
                        }
                    }
                }
            });

            this.killDeathsAssistsChart = new Chart('killDeathsAssistsChart', {
                type: 'bar',
                data: {
                    labels: ["Kills", "Deaths", "Asissts"],
                    datasets: [{
                        data: [player_kills, player_deaths, player_assists],
                        backgroundColor: [
                            'rgba(60, 209, 60, 1)',
                            'rgba(209, 60, 60, 1)',
                            '#DF691A'
                        ],
                        borderColor: [
                            '#FFF',
                            '#FFF',
                            '#FFF',
                        ],
                        borderWidth: 1
                    }]
                },
                options: {
                    responsive: true,
                    legend: {
                        display: false,
                    },
                    scales: {
                        yAxes: [{
                            ticks: {
                                beginAtZero:true
                            }
                        }]
                    },
                }
            });

            this.winLosesChart = new Chart('winLosesChart', {
                type: 'pie',
                data: {
                    labels: ["Wins", "Loses"],
                    datasets: [{
                        data: [player_wins, player_loses],
                        backgroundColor: [
                            'rgba(60, 209, 60, 1)',
                            'rgba(209, 60, 60, 1)'
                        ],
                        borderWidth: 1
                    }]
                },
                options: {
                    responsive: true,
                    legend: {
                        display: false,
                    },
                }
            });

            this.getChampions();
        });
    }

    getFreshData() {
        this.pageIsLoaded = false;
        this.url = "./assets/riotApiCommunicator.php?request=getPlayer&summoner_name="+this.name+"&region="+this.region+"&getPlayerType=pobierejpan";

        this.http.get<SinglePlayerContainer>(this.url).subscribe(resp => {
            console.log(resp);
            this.sPlayer = resp;
            window.location.reload(true);
        });
    }

    getChampions() {
        this.url = "./assets/riotApiCommunicator.php?request=championList";

        this.http.get<Champion[]>(this.url).subscribe(resp => {
            this.champions = resp;
            this.pageIsLoaded = true;
        });
    }

    getAvatarImage(avatarId: number): string {
        return "http://ddragon.leagueoflegends.com/cdn/"+this.version+"/img/profileicon/"+avatarId+".png";
    }

    getChampionName(champId: number): string {
        if(this.champions != null){
            for(let champ of this.champions) {
                if(champ.id == champId) {
                    return champ.key;
                }
            }
        }
    }

    getChampionNameImage(champId: number): string {
        if(this.champions != null){
            for(let champ of this.champions) {
                if(champ.id == champId) {
                    return "http://ddragon.leagueoflegends.com/cdn/"+this.version+"/img/champion/"+champ.key+".png";
                }
            }
        }
    }

    getItemImage(itemId: number): string {
        return "http://ddragon.leagueoflegends.com/cdn/"+this.version+"/img/item/"+itemId+".png";
    }

    getSummonerSpellImage(spellId: number): string {
        return "http://ddragon.leagueoflegends.com/cdn/"+this.version+"/img/spell/"+this.summonerSpells[spellId]+".png";
    }

    getSummonerGoldInThousands(gold: number): string {
        if(gold > 1000)
        {
            let str = gold.toString();

            str = str.slice(0, -2);
            let last_num = str.substr(str.length-1);
            str = str.slice(0, -1);
            return str+","+last_num+"k";
        }
        else
        {
            return gold.toString();
        }
    }

    getTeamKillsAsistsDeaths(players: any, team: number): string {
        let player_kills = 0;
        let player_assists = 0;
        let player_deaths = 0;

        for(let player of players)
        {
            if(player.teamId == team)
            {
                player_kills += player.stats.kills;
                player_deaths += player.stats.kills;
                player_assists += player.stats.assists;
            }
        }
        return player_kills+" / "+player_deaths+" / "+player_assists;
    }

    getGameTypeName(queueId: number): string {
        return this.typeGame[queueId];
    }

    userChanegedPlayer(name: string) {
        //this.router.navigate(['search/region/'+this.region+'/name/'+name]);();
        this.pageIsLoaded = false;
        this.sopsonDestroyData();
        this.sopsonOnInit(name);
    }

    addThisPlayerToCompareBox()
    {
        if(this.compareTwoPlayers.player1.summonerName == "")
        {
            this.compareTwoPlayers.player1.summonerName = this.name;
            this.compareTwoPlayers.player1.summonerRegion = this.region;
        }
        else
        {
            if(this.compareTwoPlayers.player2.summonerName == "")
            {
                this.compareTwoPlayers.player2.summonerName = this.name;
                this.compareTwoPlayers.player2.summonerRegion = this.region;
            }
        }

        this.recivedPlayers.changeMessage(this.compareTwoPlayers);
    }

    sopsonDestroyData() {
        this.positionsChart = [];
        this.winLosesChart = [];
        this.killDeathsAssistsChart = [];
        this.sPlayer = null;
    }
}
