import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PageItemsListComponent } from './page-items-list.component';

describe('PageItemsListComponent', () => {
  let component: PageItemsListComponent;
  let fixture: ComponentFixture<PageItemsListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PageItemsListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PageItemsListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
